import sys
import functions as func

def main(name):
    fm = func.get_masterkey()
    listPair = func.getAllPair(fm)

    number = ""

    for pair in listPair:
        if pair[0] == name:
            number = pair[1]

    if number != "":
        print("The number of " + name + " is " + number + ".\n")
    else:
        print(name + " don't find in database.\n")


if __name__ == "__main__":  
    name = sys.argv[1]
    main(name)