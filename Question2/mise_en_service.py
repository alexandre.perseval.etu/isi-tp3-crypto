import os
import shutil
from cryptography.fernet import Fernet


def deleteAllRam():
    #On supprime les fichiers de la mémoire

    os.chdir("./ramdisk")
    for i in range(4):
        if os.path.exists("pass" + str(i)):
            os.remove("pass" + str(i))
        if os.path.exists("key" + str(i) + ".pem"):
            os.remove("key" + str(i) + ".pem")
    if os.path.exists("masterkey.pem"):
        os.remove("masterkey.pem")
    
    os.chdir("..")

def stockRam():
    # On déplace les clées et les mots de passe vers la RAM pour éxécuter les tests de connexion dedans
    for i in range(4):
        if os.path.exists("./usb" + str(i)):
            shutil.copyfile("usb" + str(i) + "/key" + str(i) + ".pem", "ramdisk/key" + str(i) + ".pem")
            shutil.copyfile("usb" + str(i) + "/pass", "ramdisk/pass" + str(i) )

def readKey(number):
    os.chdir("./ramdisk")
    f = None
    if os.path.exists("key" + number + ".pem"):
        with open("key" + number + ".pem","rb") as fileKey:
            key = fileKey.read()
        f = Fernet(key)
    os.chdir("..")
    return f

def decryptMasterKey(encryptedmasterkey, keys):
    middlekey = keys[1].decrypt(encryptedmasterkey)
    masterkey = keys[0].decrypt(middlekey)
    return masterkey

def recupMasterkey(keys):

    os.chdir("./ramdisk")
    with open("masterkey_encrypted.pem","rb") as fileMaster:
        i = 0
        find = False
        encryptedmasterkey = fileMaster.readline()

        while (encryptedmasterkey != b"") and not find:
            try:
                decrypt = decryptMasterKey(encryptedmasterkey, keys)
                find = True
            except: 
                encryptedmasterkey = fileMaster.readline()
                i += 1
    os.chdir("..")

    if find:
        return (decrypt, i)
    else:
        print("There are not the good usb stick.")
        deleteAllRam()
        exit()


def main():
    os.chdir("./Server")
    stockRam()

    #On recupere les keys

    key0 = readKey("0")
    key1 = readKey("1")
    key2 = readKey("2")
    key3 = readKey("3")

    keys = []
    if (not key0 is None):
        keys.append(key0)
    if (not key1 is None):
        keys.append(key1)
    if (not key2 is None):
        keys.append(key2)
    if (not key3 is None):
        keys.append(key3)

    if len(keys) != 2:
        print("They are no 2 usb stick connected.")
        deleteAllRam()
        exit()


    #On recupere la master key
    masterkey, i = recupMasterkey(keys)

    fm = Fernet(masterkey)
    resp0 = ("0", "responsable") 
    repr0 = ("1", "représentant") 
    resp1 = ("2", "responsable")
    repr1 = ("3", "représentant") 

    if i == 0:
        #Les 2 responsables
        user0 = resp0 
        user1 = resp1
    elif i == 1:
        #Representant 0 et responsable 1
        user0 = repr0 
        user1 = resp1
    elif i == 2:
        #Representant 1 et responsable 0
        user0 = resp0 
        user1 = repr1
    elif i == 3:
        #Les 2 représentants
        user0 = repr0 
        user1 = repr1

    #Si le déchiffrement de la masterkey est réussi, on check les mots de passes
    
    os.chdir("./ramdisk")
    with open("pass" + user0[0],"rb") as filePass0:
        decrypted_p0 = fm.decrypt(filePass0.read()) 
    with open("pass" + user1[0],"rb") as filePass1:
        decrypted_p1 = fm.decrypt(filePass1.read()) 

    p0 = input("Entrez le mot de passe du " + user0[1] + " 0 : ")
    if(p0 != decrypted_p0.decode()) :
        print("Mot de passe incorrect")
        os.chdir("..")
        deleteAllRam()
        exit()

    p1 = input("Entrez le mot de passe du " + user1[1] + " 1 : ")
    if(p1 != decrypted_p1.decode()) :
        print("Mot de passe incorrect")
        os.chdir("..")
        deleteAllRam()
        exit()

    print("Mise en service réussie")

    with open("masterkey.pem","wb") as fileMaster:
        fileMaster.write(masterkey)

    os.chdir("../..")


if __name__ == "__main__":
    main()