import sys
import os
import functions as func
import add_data as add

def main(name, number):
    fm = func.get_masterkey()
    listPair = func.getAllPair(fm)

    find = False
    tupleDel = (name, number)

    for pair in listPair:
        if pair == tupleDel:
            find = True

    if find:
        listPair.remove(tupleDel)
        os.remove("./Server/dd/data")
        for pair in listPair:
            add.main(pair)

        print( name + " with number " + number + " deleted from database.\n")
    else:
        print(name + " don't find in database.\n")


if __name__ == "__main__":  
    name = sys.argv[1]
    number = sys.argv[2]
    main(name, number)