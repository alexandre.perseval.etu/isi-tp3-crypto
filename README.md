# TP 3 ISI _ Master 1 E-Services Gr2

###### PERSEVAL Alexandre
###### VALET Tristan

---

```
    Pour representer chacun des matériels, un dossier 'Server' est créé.
    Dans ce dernier, 'dd' correspond au disque dur du serveur.
    'ramdisk' à la RAM du serveur.
    'usb*' à la clé usb numéro * connecté au serveur.
```

---

## Partie 1

Ici, il a été adopté de suivra la démarche suivante pour correspondre au instructions du TP.

Trois clés de cryptage ont été généré pour la clé USB du responsable 0, la clé USB du responsable 1 et enfin la clé principale permettant de décrypter les données de la base de données.
Nommons les respectivement __keyResp0__, __keyResp1__ et __keyMaster__ pour la suite.

La __keyMaster__ est d'abord encrypté avec __keyResp0__ pour obtenir une __middleKeyMaster__ qu'on réencrypte encore avec  __keyResp1__ pour enfin obtenir enfin __encryptedKeyMaster__. Cette dernière sera alors stocké dans la RAM du serveur.

Chaque clé des responsables, __keyResp0__ et __keyResp1__, sont alors stockés sur leur clés USB avec leur mot de passe individuel qui sera crypté avec la __keyMaster__.

Lors de la mise en service, les 2 clés USB seront connectés. Le service va alors essayer de décrypter l'__encryptedKeyMaster__ à partir des clés connectés en suivant l'ordre inverse correspondant à l'encryptage.

La __keyMaster__obtenu servira a authentifier ensuite les mots de passes de chacun en les décryptant et en requetant les responsable de leur mot de passe.

Si cela correspond, on peut stocker la __keyMaster__ sur la RAM. A contrario, si cela est faux on supprime la RAM actuelle.

Pour les différentes services , on regarde à chaque fois si __keyMaster__ est stocké dans la RAM si cela est le cas on l'utilisen, sinon cela signifie que le service n'est pas disponible.

---

## Partie 2

On utilise ici la même méthode que précedemment.

Cependant, on crée 2 nouvelles clés __keyRepr0__ et __keyRepr1__ pour les représentant et leur mot de passe seront encryptés par la __keyMaster__.

On va alors encrypter la __keyMaster__ avec les différentes combinaisons de responsables / responsable.
On stocke toutes ces __encryptedKeyMaster__ sur la RAM.

A la mise en service, 2 clés sont alors connectés et on test toutes nos __encryptedKeyMaster__. 
Si cela fonctionne on passe à la phase des mots de passes, sinon on quitte et on vide la RAM.

```
    Dans l'initialisation de la partie 2 et 3, les clés usb 0, 1, 2 et 3 sont créés directementdans 'Server'.

    0 => Responsable 0
    1 => Représentant 0
    2 => Responsable 1
    3 => Représentant 1 

    Pour simuler le fait d'avoir 2 clés USB connecté, il faut supprimer(ou déplacer) 2 dossiers 'usb*'.
    Si les 4 dossiers reste présent, la simulation penseras que 4 clés sont connectés.
    IL FAUT IMPERATIVEMENT EN AVOIR STRICTEMENT 2 DE CONNECTER.
```

Les différentes services fonctionnent exactement de la même manière.

---

## Partie 3

Ici on va simplement supprimer les combinaisons d'__encryptedKeyMaster__ auxquelles la personne répudiée est associé.

On ne pourra donc plus se connecter avec la personne répudiée.
