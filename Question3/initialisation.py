import os
import shutil
from cryptography.fernet import Fernet

def generateKey(number):
    os.mkdir("usb" + number)
    os.chdir("./usb" + number)
    key = Fernet.generate_key()
    with open("key" + number + ".pem","wb") as fileKey0:
        fileKey0.write(key)
    
    os.chdir("..")
    return key

def generateEncryptedMasterKey(masterkey, userkeys):
    middlekey = userkeys[0].encrypt(masterkey)
    encryptedmasterkey = userkeys[1].encrypt(middlekey)
    return encryptedmasterkey

def cryptPassword(password, number, fernet):
    os.chdir("./usb" + number)
    pw = fernet.encrypt(password)
    with open("../usb" + number + "/pass","wb") as filePass:
        filePass.write(pw)
    os.chdir("..")

def main():
    #Création de l'arborescence du serveur
    os.mkdir("Server")
    os.chdir("Server")

    os.mkdir("dd")
    os.mkdir("ramdisk")

    #Génération des clées

    key0 = generateKey("0") #Responsable 0
    key1 = generateKey("1") #Representant du responsable 0
    key2 = generateKey("2") #Responsable 1
    key3 = generateKey("3") #Representant du responsable 1

    # Representation des possibilités :
    #     0101 => les 2 responsables
    #     0110 => Representant 0 et responsable 1
    #     1001 => Representant 1 et responsable 0
    #     1010 => les 2 représentants

    #Génération de la master key à partir des deux clées

    os.chdir("./ramdisk")

    f0 = Fernet(key0)
    f1 = Fernet(key1)
    f2 = Fernet(key2)
    f3 = Fernet(key3)

    masterkey = Fernet.generate_key()

    encryptedmaster0101 = generateEncryptedMasterKey(masterkey, [f0, f2])
    encryptedmaster0110 = generateEncryptedMasterKey(masterkey, [f1, f2])
    encryptedmaster1001 = generateEncryptedMasterKey(masterkey, [f0, f3])
    encryptedmaster1010 = generateEncryptedMasterKey(masterkey, [f1, f3])

    with open("masterkey_encrypted.pem","wb") as fileMaster:
        fileMaster.write(encryptedmaster0101 + b"\n")
        fileMaster.write(encryptedmaster0110 + b"\n")
        fileMaster.write(encryptedmaster1001 + b"\n")
        fileMaster.write(encryptedmaster1010 + b"\n")

    
    os.chdir("..")

    #On crypte les mots de passes des deux responsables avec la masterkey
    fm = Fernet(masterkey)
    
    cryptPassword(b"abc", "0", fm)
    cryptPassword(b"aze", "1", fm)
    cryptPassword(b"123", "2", fm)
    cryptPassword(b"789", "3", fm)

if __name__ == "__main__":
    main()