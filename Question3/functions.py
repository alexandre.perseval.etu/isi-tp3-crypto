import os
from cryptography.fernet import Fernet

def get_masterkey():
    os.chdir("./server/ramdisk")
    if os.path.exists("masterkey.pem"):
        with open("masterkey.pem", 'rb') as fileMaster:
            key = fileMaster.read()
        os.chdir("../..")
        return Fernet(key)
    else:
        os.chdir("../..")
        print("The service don't work.\nPlease, try to launch the service first.\n")
        exit()

def getAllPair(fernet):
    os.chdir("./server/dd")
    
    if os.path.exists("data"):
        listPair = []
        with open("data", 'rb') as data:
            line = data.readline()
            while (line != b""):
                decrypted = fernet.decrypt(line).decode()
                pair = tuple(decrypted.strip().split(":::"))
                listPair.append(pair)
                line = data.readline()
        
        os.chdir("../..")
        return listPair
    else:
        os.chdir("../..")
        print("They are nothing in data base.\n")
        exit()


if __name__ == "__main__":  
    print(getAllPair(get_masterkey()))