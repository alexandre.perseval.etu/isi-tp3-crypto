import sys
import os
import functions as func

#     ligne 0 => les 2 responsables
#     ligne 1 => Representant 0 et responsable 1
#     ligne 2 => Representant 1 et responsable 0
#     ligne 3 => les 2 représentants

# Virer:
#     Responsable 0 : 0  2
#     Representant 0 : 1  3
#     Responsable 1 : 0  1
#     Representant 1 : 2  3 

def main():
    func.get_masterkey()
    resp0 = (0, 2, b"responsable 0") 
    repr0 = (1, 3, b"representant 0") 
    resp1 = (0, 1, b"responsable 1")
    repr1 = (2, 3, b"representant 1") 

    text = "Quel utilisateur répudier ?:\n" +\
        "\t0 => Responsable 0\n" +\
        "\t1 => Représentant 0\n" +\
        "\t2 => Responsable 1\n" +\
        "\t3 => Représentant 1\n"

    who = -1
    while who < 0 or who > 3:
        who = int(input(text))
        if who < 0 and who > 3:
            print("No user with " + who + " id.\nPlease try again.\n" )

    if who == 0:
        user = resp0
    elif who == 1:
        user = repr0
    elif who == 2:
        user = resp1
    elif who == 3:
        user = repr1

    os.chdir("./Server/ramdisk")
    with open("masterkey_encrypted.pem","rb") as fileMaster:
        tmp = b""
        for i in range(4):
            if i != user[0] and i != user[1]:
                tmp += fileMaster.readline()
            else:
                fileMaster.readline()
                tmp += b"Not Allowed, " + user[2] + b" fired.\n"
    
    with open("masterkey_encrypted.pem","wb") as fileMaster:
        fileMaster.write(tmp)
    
    os.chdir("../..")

    


if __name__ == "__main__":
    main()