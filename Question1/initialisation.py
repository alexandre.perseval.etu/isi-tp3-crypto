import os
import shutil
from cryptography.fernet import Fernet

def main():
    #Création de l'arborescence du serveur
    os.mkdir("Server")
    os.chdir("Server")

    os.mkdir("dd")
    os.mkdir("ramdisk")
    os.mkdir("usb0")
    os.mkdir("usb1")

    #Génération des clées des responsables

    os.chdir("usb0")
    key0 = Fernet.generate_key()
    with open("key0.pem","wb") as fileKey0:
        fileKey0.write(key0)

    os.chdir("../usb1")
    key1 = Fernet.generate_key()
    with open("key1.pem","wb") as fileKey1:
        fileKey1.write(key1)

    #Génération de la master key à partir des deux clées

    os.chdir("../ramdisk")

    f0 = Fernet(key0)
    f1 = Fernet(key1)
    masterkey = Fernet.generate_key()
    middlekey = f0.encrypt(masterkey)
    encryptedmasterkey = f1.encrypt(middlekey)
    with open("masterkey_encrypted.pem","wb") as fileMaster:
        fileMaster.write(encryptedmasterkey)

    #On crypte les mots de passes des deux responsables avec la masterkey
    fm = Fernet(masterkey)
    pass0 = fm.encrypt(b"abcdef")
    with open("../usb0/pass","wb") as filePass0:
        filePass0.write(pass0)
    pass1 = fm.encrypt(b"123456")
    with open("../usb1/pass","wb") as filePass1:
        filePass1.write(pass1)


if __name__ == "__main__":
    main()