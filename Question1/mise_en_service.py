import os
import shutil
from cryptography.fernet import Fernet


def deleteAllRam():
    #On supprime les fichiers de la mémoire
    
    if os.path.exists("pass0"):
        os.remove("pass0")
    if os.path.exists("pass1"):
        os.remove("pass1")
    if os.path.exists("key0.pem"):
        os.remove("key0.pem")
    if os.path.exists("key1.pem"):
        os.remove("key1.pem")
    if os.path.exists("masterkey.pem"):
        os.remove("masterkey.pem")

def stockRam():
    # On déplace les clées et les mots de passe vers la RAM pour éxécuter les tests de connexion dedans
    shutil.copyfile("usb0/key0.pem","ramdisk/key0.pem")
    shutil.copyfile("usb0/pass","ramdisk/pass0")
    shutil.copyfile("usb1/key1.pem","ramdisk/key1.pem")
    shutil.copyfile("usb1/pass","ramdisk/pass1")


def main():
    os.chdir("./Server")
    stockRam()

    #On déchiffre la master-key avec les deux clées

    os.chdir("./ramdisk")
    with open("key0.pem","rb") as fileKey0:
        key0 = fileKey0.read()
    f0 = Fernet(key0)

    with open("key1.pem","rb") as fileKey1:
        key1 = fileKey1.read()
    f1 = Fernet(key1)

    with open("masterkey_encrypted.pem","rb") as fileMaster:
        encryptedmasterkey = fileMaster.read()
    middlekey = f1.decrypt(encryptedmasterkey)
    masterkey = f0.decrypt(middlekey)

    fm = Fernet(masterkey)

    #Si le déchiffrement de la masterkey est réussi, on check les mots de passes
    with open("pass0","rb") as filePass0:
        decrypted_p0 = fm.decrypt(filePass0.read()) 
    with open("pass1","rb") as filePass1:
        decrypted_p1 = fm.decrypt(filePass1.read()) 

    p0 = input("Entrez le mot de passe du responsable 0 : ")
    if(p0 != decrypted_p0.decode()) :
        print("Mot de passe incorrect")
        deleteAllRam()
        os.chdir("../..")
        exit()

    p1 = input("Entrez le mot de passe du responsable 1 : ")
    if(p1 != decrypted_p1.decode()) :
        print("Mot de passe incorrect")
        deleteAllRam()
        os.chdir("../..")
        exit()

    print("Mise en service réussie")

    with open("masterkey.pem","wb") as fileMaster:
        fileMaster.write(masterkey)

    os.chdir("../..")


if __name__ == "__main__":
    main()