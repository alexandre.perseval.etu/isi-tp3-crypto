import os
import sys
import functions as func

def createLineData(pair):
    return (':::'.join(pair)).encode()

def main(args):
    fm = func.get_masterkey()

    os.chdir("./server/dd")

    dataLine = fm.encrypt(createLineData(args))
    with open("data", "ab") as data:
        data.write(dataLine + b"\n")
    
    os.chdir("../..")



if __name__ == "__main__":  
    args = sys.argv[1:]
    main(args)